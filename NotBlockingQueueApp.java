package notblockingqueue;

import java.util.LinkedList;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.atomic.AtomicReference;

interface CallbackInterface<T>
{

    void callback(T param);
}

class Notifier
{

    boolean requestedCallback = false;
    final AtomicReference<LinkedList<CallbackInterface>> data
            = new AtomicReference(new LinkedList());

    void addSubscriber(CallbackInterface callback)
    {
        LinkedList cloneData;
        LinkedList clone2Data;
        do
        {
            cloneData = (LinkedList) data.get();
            clone2Data = (LinkedList) cloneData.clone();
            clone2Data.addLast(callback);
        } while (!data.compareAndSet(cloneData, clone2Data));
        if (requestedCallback)
        {
            requestedCallback = false;
            pollFirstAndUseCallback();
        }
    }

    void pollFirstAndUseCallback()
    {
        if (data.get().isEmpty())
        {
            requestedCallback = true;
            return;
        }
        CallbackInterface callback;
        LinkedList cloneData;
        LinkedList clone2Data;
        do
        {
            cloneData = (LinkedList) data.get();
            clone2Data = (LinkedList) cloneData.clone();
            callback = (CallbackInterface) clone2Data.pollFirst();
        } while (!data.compareAndSet(cloneData, clone2Data));
        callback.callback(null);
    }
}

class NotBlockingQueue<T>
{

    final Notifier waitingToPop = new Notifier();
    final Notifier waitingToPush = new Notifier();

    final AtomicReference<LinkedList<T>> data
            = new AtomicReference(new LinkedList());
    int maximalCapacity = 0;

    NotBlockingQueue(int maximalCapacity)
    {
        this.maximalCapacity = maximalCapacity;
    }

    boolean isEmpty()
    {
        return data.get().isEmpty();
    }

    boolean isFull()
    {
        return size() >= maximalCapacity;
    }

    int size()
    {
        return data.get().size();
    }

    void push(T item, CallbackInterface callback)
    {
        LinkedList cloneData;
        LinkedList clone2Data;
        do
        {
            cloneData = (LinkedList) data.get();
            clone2Data = (LinkedList) cloneData.clone();
            clone2Data.addLast(item);
            if (isFull())
            {
                waitingToPush.addSubscriber((param) ->
                {
                    this.push(item, callback);
                });
                return;
            }
        } while (!data.compareAndSet(cloneData, clone2Data));
        callback.callback(item);
        waitingToPop.pollFirstAndUseCallback();
    }

    void pop(CallbackInterface callback)
    {
        T result;
        LinkedList cloneData;
        LinkedList clone2Data;
        do
        {
            cloneData = (LinkedList) data.get();
            clone2Data = (LinkedList) cloneData.clone();
            result = (T) clone2Data.pollFirst();
            if (isEmpty())
            {
                waitingToPop.addSubscriber((param) ->
                {
                    this.pop(callback);
                });
                return;
            }
        } while (!data.compareAndSet(cloneData, clone2Data));
        callback.callback(result);
        waitingToPush.pollFirstAndUseCallback();
    }

    void show()
    {
        System.out.print(data.get());
    }
}

public class NotBlockingQueueApp
{

    public static void main(String[] args)
    {
        NotBlockingQueue q = new NotBlockingQueue(10);
        ForkJoinPool pool = ForkJoinPool.commonPool();

        for (int i = 0; i < 10; i++)
        {
            pool.submit(() ->
            {
                q.pop((result) ->
                {
                    System.out.println("Got second value " + result);
                });
                System.out.println("Asked to get second value");
            });
        }

        for (int i = 0; i < 15; i++)
        {
            final int i_copy = i;
            pool.submit(() ->
            {
                System.out.println("A " + i_copy);
                q.push(i_copy, (result) ->
                {
                    System.out.println("I " + result);
                });
            });
        }


        try
        {
            Thread.sleep(2000);
        } catch (InterruptedException e)
        {
        } finally
        {
            q.show();
            System.out.println(q.size());
        }
    }
}
